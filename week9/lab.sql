SELECT * FROM Customers;

UPDATE Customers SET Country = replace(Country, '\n', '');

UPDATE Customers SET City = replace(City, '\n', '');

Create view mexicanCustomers as
SELECT CustomerID,CustomerName,ContactName FROM Customers where Country = 'Mexico';

SELECT * from mexicanCustomers;

SELECT * from mexicanCustomers join Orders on mexicanCustomers.CustomerID=Orders.CustomerID;

create view productsbelowavg as
SELECT ProductID,ProductName, Price from Products where price < (select avg(Price) from Products);

DELETE FROM OrderDetails WHERE ProductID = 5;

truncate OrderDetails;

delete from Customers;
delete from Orders;

drop table Customers;